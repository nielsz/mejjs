package com.example.mejjs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.mejjs.presenters.MainPresenter;

public class MainActivity extends AppCompatActivity {

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter();
        presenter.start();
    }
}
